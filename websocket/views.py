from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
#from django.utils.decorators import method_decorator
import json
from . import models
import boto3
# Create your views here.

def _parse_body(body):
    body_unicode = body.decode('utf-8')
    return json.loads(body_unicode)

@csrf_exempt
def test(request):
    return JsonResponse({'message': 'hello Daud'}, status=200)

@csrf_exempt
def connect(request):
    body = _parse_body(request.body)
    models.Connection.objects.create(connection_id = body['connectionId'])
    return JsonResponse({'message': 'connect successfully'}, status=200)

@csrf_exempt
def disconnect(request):
    body = _parse_body(request.body)
    models.Connection.objects.get(connection_id = body['connectionId']).delete()
    return JsonResponse({'message': 'disconnect successfully'}, status=200)

@csrf_exempt
def _send_to_connection(connection_id, data):
    gatewayapi=boto3.client('apigatewaymanagementapi', endpoint_url='https://0h49agnmb9.execute-api.us-east-2.amazonaws.com/test/',
    region_name='us-east-2', 
    aws_access_key_id ='AKIAXOYP3RI6CRTNVZH7', aws_secret_access_key='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')
    return gatewayapi.post_to_connection(ConnectionId= connection_id, Data=json.dumps(data).encode('utf-8'))

@csrf_exempt
def send_message(request):
    body = _parse_body(request.body)
    models.ChatMessage.objects.create(username = body["body"]["username"], message= body["body"]["content"], timestamp = body["body"]["timestamp"])
    connections = models.Connection.objects.all()
    data = {'messages':[body]}
    for connection in connections:
        _send_to_connection(connection.connection_id, data)
    return JsonResponse({"message": "Message sent successfully"}, status= 200)
    
@csrf_exempt
def recent_messages(request):
    body = _parse_body(request.body)
    id = models.Connection.objects.get(connection_id = body['connectionId'])
    messages = list(models.ChatMessage.objects.values("username", "message", "timestamp"))
    data = {'messages': messages}
    _send_to_connection(id.connection_id, data)
    return JsonResponse(data, status=200)



    #wscat -c wss://0h49agnmb9.execute-api.us-east-2.amazonaws.com/test/
    #{"action":"getRecentMessages"}
    #{"action":"sendMessage", "username":"charles","content":"Hi everyone","timestamp":"7pm"}