# Generated by Django 3.0.6 on 2020-05-15 03:03

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('charlesscrumy', '0004_auto_20200515_0253'),
    ]

    operations = [
        migrations.AlterField(
            model_name='scrumygoals',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='maps', to=settings.AUTH_USER_MODEL),
        ),
    ]
