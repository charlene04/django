from django.apps import AppConfig


class CharlesscrumyConfig(AppConfig):
    name = 'charlesscrumy'
