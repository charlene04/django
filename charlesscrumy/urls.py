
from charlesscrumy import views
from django.contrib.auth.views import LogoutView
from django.contrib.auth.views import  LoginView
from django.urls import path, include
app_name = 'charlesscrumy'
urlpatterns = [
    path('', views.index, name='index'),
   path('movegoal/<int:goal_id>/', views.move_goal, name='movegoal'),
    path('addgoal/', views.add_goal, name='addgoal'),
    path('home/', views.home, name='home'),
    path('accounts/', include('django.contrib.auth.urls')),
    path("logout/", LogoutView.as_view(), name="logout"),
]
