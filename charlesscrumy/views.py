from django.shortcuts import render, redirect
# Create your views here.
from django.http import HttpResponse
from . import models
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from . import form
from django.contrib.auth import login
from django.contrib.auth import authenticate
GroupDeveloper = Group.objects.get(name = 'Developer') 
GroupOwner = Group.objects.get(name = 'Owner')
GroupQualityAssurance = Group.objects.get(name = 'Quality Assurance')
GroupAdmin = Group.objects.get(name = 'Admin')
import random
idList = []


#from myapp.models import Book
#from django.contrib.auth.models import Group, Permission
#from django.contrib.contenttypes.models import ContentType content_type = ContentType.objects.get_for_model(Book)
#permission = Permission.objects.create(codename='can_publish',
 #                                      name='Can Publish book',
  #                                     content_type=content_type)
#def index(request): 
   # goal = models.ScrumyGoals.objects.filter(goal_name ="Learn Django")
   # for content in goal:
    ##    html = "<html><body><h4>Goals</h4><ul><li>%s</li></ul></body></html>" %content
    #    return HttpResponse(content)



def index(request): 
     if request.method == 'POST': 
         formView = form.SignupForm(request.POST) 
         if formView.is_valid():
            #post = form.save(commit=False)
            newUser = formView.save(commit = False)
            newUser.set_password(request.POST.get('password'))
            newUser.save()
            GroupDeveloper.user_set.add(newUser)
            return HttpResponse('YOUR ACCOUNT HAS BEEN CREATED SUCCESSFULLY')
     else: 
         formView = form.SignupForm()
         return render(request, 'charlesscrumy/index.html', {'form': formView})


def move_goal(request, goal_id):
    if request.method == 'POST':
        goal = models.ScrumyGoals.objects.get(goal_id = goal_id)
        verify = models.GoalStatus.objects.get(status_name = "Verify Goal")
        done =  models.GoalStatus.objects.get(status_name = "Done Goal")
        if request.user.groups.filter(name__in=[GroupDeveloper]).exists() and goal.created_by != request.user.username or request.user.groups.filter(name__in=[GroupOwner]).exists() and goal.created_by != request.user.username:
            return render(request, 'charlesscrumy/exception.html', {'error': 'Sorry, You can only move the goal you created.'})
        elif request.user.groups.filter(name__in=[GroupDeveloper]).exists() and goal.goal_status == done:
            return render(request, 'charlesscrumy/exception.html', {'error': 'Sorry, as a developer, the Done column is none of your business!'})
        elif request.user.groups.filter(name__in=[GroupDeveloper]).exists() and request.POST.get('frequency') == 'Done Goal':
            return render(request, 'charlesscrumy/exception.html', {'error': 'Sorry, you cannot mark goal\'s status as done.'})
        elif request.user.groups.filter(name__in=[GroupQualityAssurance]).exists() and goal.goal_status != verify and goal.created_by != request.user.username and (request.POST.get('frequency') in ['Done Goal', 'Daily Goal', 'Weekly Goal', 'Verify Goal']):
            return render(request, 'charlesscrumy/exception.html', {'error': 'Sorry, as a Quality Assurer, you can only move goals of other users from Verified to Done columns.'})
        elif request.user.groups.filter(name__in=[GroupQualityAssurance]).exists() and goal.goal_status == verify and goal.created_by != request.user.username and (request.POST.get('frequency') in ['Daily Goal', 'Weekly Goal', 'Verify Goal']):
            return render(request, 'charlesscrumy/exception.html', {'error': 'Sorry, as a Quality Assurer, you can only move goals of other users from Verified to Done columns.'})
        else:  
            goal.goal_status = models.GoalStatus.objects.get(status_name = request.POST.get('frequency'))
            goal.save()
            return redirect("charlesscrumy:home")                      
    else:
        return render(request, 'charlesscrumy/movegoal.html')
   

def add_goal(request):
    if request.method == 'POST': 
        if request.user.groups.filter(name__in=[GroupAdmin]).exists():
            return render(request, 'charlesscrumy/exception.html', {'error': 'Sorry, your job is to move goals to and fro statuses.'})
        ID = []
        i = 0
        while i < len(idList) + 1:
            rnd = random.randint(1000 , 9999)
            if rnd in idList:
                continue
            ID.append(rnd)
            i = i+ 1
        requiredID = ID[0]
        idList.append(requiredID)
    
        models.ScrumyGoals.objects.create(goal_name = request.POST.get('goal_name'), 
                                        goal_id = requiredID,
                                        created_by = request.user.username, 
                                        moved_by = request.user.username,
                                        owner = request.user.username,
                                        goal_status =  models.GoalStatus.objects.get(status_name = "Weekly Goal"), 
                                        user = User.objects.get(username = request.POST.get('user')))
        return redirect("charlesscrumy:home") 
    else:
        if request.user.groups.filter(name__in=[GroupDeveloper, GroupQualityAssurance, GroupOwner]).exists():
            return render(request, 'charlesscrumy/addgoal.html', {'users': User.objects.filter(first_name = request.user.first_name)})
        else:
            return render(request, 'charlesscrumy/addgoal.html', {'users': User.objects.all()})

                
def home(request):
    context = {'users': User.objects.all(), 
                'weeklyGoals': models.ScrumyGoals.objects.filter(goal_status = models.GoalStatus.objects.get(status_name = "Weekly Goal")),
                'dailyGoals': models.ScrumyGoals.objects.filter(goal_status = models.GoalStatus.objects.get(status_name = "Daily Goal")),
                'verifyGoals': models.ScrumyGoals.objects.filter(goal_status = models.GoalStatus.objects.get(status_name = "Verify Goal")),
                'doneGoals': models.ScrumyGoals.objects.filter(goal_status = models.GoalStatus.objects.get(status_name = "Done Goal"))}
    
    return render(request, 'charlesscrumy/home.html', context)



    

def logout_request(request):
    logout(request)
    return redirect("charlesscrumy:index")