=============
charlesscrumy
------------------------


charlesscrumy is a simple Django app to display a welcome screen on visit to the website url.

Quick start
-------------
1. Add "charlesscrumy" to your INSTALLED_APPS setting like this:
      
         INSTALLED_APPS = [
             ....
             'charlesscrumy',
         ]
    
2. Include the charlesscrumy URLconf in your project urls.py like this:
   
     path('/charlesscrumy', Include('charlesscrumy.urls')),


3. Run 'python manage.py migrate' to create the charlesscrumy models


4. Start the development server and visit http://54.177.42.195:8000/charlesscrumy to use the site.

