from django.contrib import admin
from . import models
# Register your models here.

admin.site.register(models.ScrumyGoals)
admin.site.register(models.ScrumyHistory)
admin.site.register(models.GoalStatus)