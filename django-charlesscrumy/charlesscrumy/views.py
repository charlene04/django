from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse 
from . import models
from django.contrib.auth.models import User

import random
idList = []



def index(request): 
    goal = models.ScrumyGoals.objects.filter(goal_name ="Learn Django")
    for content in goal:
        html = "<html><body><h4>Goals</h4><ul><li>%s</li></ul></body></html>" %content
        return HttpResponse("<html><body><h1>This is a Scrum Application</h1></body></html>\n\n %s" %html)



def move_goal(request, goal_id):
    context = {'error': 'A record with that goal id %d does not exist.' %goal_id}
    try: goal = models.ScrumyGoals.objects.get(goal_id= goal_id) 
    except Exception as e: return render(request, 'charlesscrumy/exception.html', context) 
    else: return HttpResponse(goal.goal_name)

def add_goal(request):
    ID = []
    i = 0
    while i < len(idList):
        rnd = random.randint(1000 , 9999)
        if rnd in idList:
            continue
        ID.append(rnd)
        i = i+ 1
    requiredID = ID[0]
    idList.append(requiredID)
    
    models.ScrumyGoals.objects.create(goal_name = 'Keep Learning Django', 
                                        goal_id = requiredID,
                                        created_by = 'Louis', 
                                        moved_by = 'Louis',
                                        owner = 'Louis',
                                        goal_status =  models.GoalStatus.objects.get(status_name = "Weekly Goal"), 
                                        user = User.objects.get(username = 'louis'))

                
def home(request):
    context = {'users': User.objects.all(), 
                'weeklyGoals': models.ScrumyGoals.objects.filter(goal_status = models.GoalStatus.objects.get(status_name = "Weekly Goal")),
                'dailyGoals': models.ScrumyGoals.objects.filter(goal_status = models.GoalStatus.objects.get(status_name = "Daily Goal")),
                'verifyGoals': models.ScrumyGoals.objects.filter(goal_status = models.GoalStatus.objects.get(status_name = "Verify Goal")),
                'doneGoals': models.ScrumyGoals.objects.filter(goal_status = models.GoalStatus.objects.get(status_name = "Done Goal"))}
    #return HttpResponse("%s" %output)
    return render(request, 'charlesscrumy/home.html', context)


    