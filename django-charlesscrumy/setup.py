import os
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

#allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='charlesscrumy',
    version= '1.0' ,
    packages = find_packages(),
    include_package_data = True,
    description = 'A simple Django app to display a welcome screen.',
    long_description =README,
    url = 'http://54.177.42.195:8000/charlesscrumy',
    author = 'Charles Ugbana',
    author_email = 'charlesugbana04@gmail.com',
    classifiers = [
        'Environment :: Web Environment',
        'Framework :: Django',
        'Framework :: Django ::3.0.6',
        'Intended_Audience :: Developers',
        #'Licence :: DST Approved :: BSD Licence',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.6',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Static Content',
    ],
)